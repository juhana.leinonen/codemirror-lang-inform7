import { LanguageSupport } from "@codemirror/language"
import { StreamLanguage } from "@codemirror/stream-parser"

enum Scope {
	Title = "title",
	Body = "body",
	Text = "text",
	TextSubstitution = "textSubstitution",
	Inform6Inclusion = "inform6Inclusion",
	Inform6String = "inform6String",
}

interface State {
	currentScope: Scope,
	commentDepth: number,
}

export const inform7Language = StreamLanguage.define<State>({
	startState(indentUnit) {
		return {
			currentScope: Scope.Title,
			commentDepth: 0,
		}
	},

	token(stream, state) {
		/* Comments can appear in multiple scopes, so we check them first. */
		if (state.commentDepth) {
			/* First check if we are at the final closing bracket. */
			if (state.commentDepth === 1 && stream.eat("]")) {
				state.commentDepth = 0
				return "blockComment"
			}
			/* Scan the line and keep track of the comment depth. */
			while (!stream.eol()) {
				const next = stream.next()
				if (next === "[") {
					state.commentDepth++
				} else if (next === "]") {
					/* If it's the final bracket, go back and return. We'll handle it next time. */
					if (state.commentDepth === 1) {
						stream.backUp(1)
						return "blockComment"
					}
					state.commentDepth--
				}
			}

			/* The comment continues beyond the current line. */
			return "blockComment"

		/* The title is the first line of the source. We see if it's present and set the scope to the body anyway. */
		} else if (state.currentScope === Scope.Title) {
			state.currentScope = Scope.Body
			/* We are very lenient on the format. Any line starting with a quotation mark will do. */
			if (stream.sol() && stream.match(/".*/)) {
				return "heading1"
			}
			return null

		/* We are in the main source. */
		} else if (state.currentScope === Scope.Body) {
			/* We first check special lines. */
			if (stream.sol()) {
				/* Lines starting with a heading word. */
				if (stream.match(/^\s*volume\b.*/i)) {
					return "heading2"
				} else if (stream.match(/^\s*book\b.*/i)) {
					return "heading3"
				} else if (stream.match(/^\s*part\b.*/i)) {
					return "heading4"
				} else if (stream.match(/^\s*chapter\b.*/i)) {
					return "heading5"
				} else if (stream.match(/^\s*section\b.*/i)) {
					return "heading6"
					/* Lines starting with "Table" are table titles. */
				} else if (stream.match(/^\s*table\b.*/i)) {
					return "definitionKeyword" // TODO: Maybe not the best token tag?
				}
			}

			/* Next is a quotation mark: start a string. */
			if (stream.eat("\"")) {
				state.currentScope = Scope.Text
				return "string"
			/* Next is an opening bracket: start a block comment. */
			} else if (stream.eat("[")) {
				state.commentDepth = 1
				return "blockComment"
			/* Next is a "(-": start an Inform 6 inclusion. */
			} else if (stream.match("(-")) {
				state.currentScope = Scope.Inform6Inclusion
				return "bracket"
			}

			/* Scan the line. */
			while (!stream.eol()) {
				/* If the next characters start a new scope, stop, else continue. */
				const peeked = stream.peek()
				if (peeked === "\"" || peeked === "[" || stream.match("(-", false)) {
					return null
				} else {
					stream.next()
				}
			}
			return null

		/* We are in a text. */
		} else if (state.currentScope === Scope.Text) {
			const next = stream.next()
			if (next === "\"") {
				state.currentScope = Scope.Body
			} else if (next === "]") {
				return "invalid"
			} else if (next === "[") {
				state.currentScope = Scope.TextSubstitution
				return "escape"
			} else {
				/* Eat characters that have no signification in a text. */
				stream.eatWhile(/[^"\[\]]/)
			}
			return "string"

		/* We are in a text substitution */
		} else if (state.currentScope === Scope.TextSubstitution) {
			const next = stream.next()
			if (next === "[") {
				return "invalid"
			} else if (next === "]") {
				state.currentScope = Scope.Text
			} else if (next === "\"") {
				state.currentScope = Scope.Body
				return "string"
			} else {
				/* Eat characters that have no signification in a substitution. */
				stream.eatWhile(/[^"\[\]]/)
			}
			return "escape"

		/* We are in an Inform 6 inclusion. */
		} else if (state.currentScope === Scope.Inform6Inclusion) {
			if (stream.match("-)")) {
				state.currentScope = Scope.Body
				return "bracket"
			} else if (stream.match("!")) {
				stream.skipToEnd()
				return "lineComment"
			} else if (stream.match(/^'(@(\{[0-9a-fA-F]{1,6}\}|[`:^][aeiouAEIOU]|:y|'[aeiouyAEIOUY]|c[cC]|~[anoANO]|\/[oO]|o[aA]|ss|oe|OE|ae|AE|th|Th|et|Et|LL|!!|\?\?|<<|>>)|[^@])'/)) {
				return "integer"
			} else if (stream.match(/^'[^']+'/)) {
				return "atom"
			} else if (stream.eat("\"")) {
				state.currentScope = Scope.Inform6String
				return "string"
			} else if (stream.match(/^(box|break|continue|do|else|font\s+(on|off)|for|give|if|inversion|jump|move|new_line|objectloop|print|print_ret|quit|remove|return|rfalse|rtrue|spaces|string|switch|until|while|read|restore|save|style\s+(roman|bold|underline|reverse|fixed))\b/)) {
				return "controlKeyword"
			} else if (stream.eat(/\[|\]/)) {
				return "processingInstruction"
			} else if (stream.match(/^#?(Abbreviate|Array|Attribute|Constant|Default|Dictionary|End|Endif|Extend|Global|Ifdef|Iffalse|Ifndef|Ifnot|Iftrue|Import|Include|Link|Message(\s+(error|fatalerror|warning))?|Origsource|Property(\s+additive)?|Release|Replace|Serial|StatusLine\s+(time|score)|Stub|Switches|System_file|Undef|Verb(\s+meta)?|Zcharacter(\s+table)?|Fake_action|Ifv3|Ifv5|Lowstring|Nearby|Trace)\b/i)) {
				return "processingInstruction"
			} else if (stream.match(/^(class|has|hasnt|in|notin|ofclass|or|private|provides|with)\b/) ) {
				return "operatorKeyword"
			} else if (stream.match(/^(true|false)\b/) ) {
				return "bool"
			} else if (stream.match(/^nothing\b/) ) {
				return "null"
			} else if (stream.match(/^self\b/) ) {
				return "self"
			} else if (stream.match(/^\$\$[01]+\b/)) {
				return "integer"
			} else if (stream.match(/^\$[0-9a-fA-F]+\b/)) {
				return "integer"
			} else if (stream.match(/^\$[+-]\d+(\.\d+)?(e[+-]\d+)?\b/)) {
				return "float"
			} else if (stream.match(/^\d+\b/)) {
				return "integer"
			} else if (stream.match(/^\w+\b/)) {
				return null
			} else {
				/* Eat the whitespace, or else at least advance one character. */
				if (!stream.eatSpace()) {
					stream.next()
				}
				return null
			}

		} else if (state.currentScope === Scope.Inform6String) {
			if (stream.eat("\"")) {
				state.currentScope = Scope.Inform6Inclusion
			} else if (stream.match(/^(\^|~|\\|@(\{[0-9a-fA-F]+\}|@\d{1,3}|[\^`:][aeiouAEIOU]|:y|'[aeiouyAEIOUY]|c[cC]|~[anoANO]|\/[oO]|o[aA]|ss|oe|ae|OE|AE|th|Th|et|Et|LL|!!|\?\?|<<|>>))/)) {
				return "escape"
			} else {
				/* Eat a least one character to be sure we advance, then eat until the next escape or the end of the string. */
				stream.next()
				stream.eatWhile(/[^"~\^@\\]/)
			}
			return "string"
		}

		/* In case of, we eat the rest of the line if everything failed due to a bug in the code above. */
		// TODO: show error.
		stream.skipToEnd()
		return null
	}
})

export function inform7() {
	return new LanguageSupport(inform7Language)
}
