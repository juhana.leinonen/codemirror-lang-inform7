import { EditorState, EditorView, basicSetup } from "@codemirror/basic-setup"

import { inform7 } from "../src/index"

const editor = new EditorView({
	state: EditorState.create({
		extensions: [
			basicSetup,
			EditorView.lineWrapping,
			inform7()
		],
	}),
	parent: document.body,
})
