# Changelog

## 0.1.0

- Initial release. Contains a basic but functional Inform 7 syntax highlighter for CodeMirror 6.
