# Inform 7 for CodeMirror

This package implements Inform 7 syntax highlighting for the CodeMirror 6 code editor.

Inform 7 extensions are not fully supported. (They are mostly fine, only extension-specific parts such as the documentation are not highlighted.)

Inform 6 inclusions have basic highlighting, and Preform inclusions are not supported at all.

## Usage

```js
import { EditorState, EditorView, basicSetup } from "@codemirror/basic-setup"

import { inform7 } from "codemirror-lang-inform7"

const editor = new EditorView({
	state: EditorState.create({
		extensions: [
			basicSetup,
			EditorView.lineWrapping,
			inform7(),
		],
	}),
	parent: document.body,
})
```

## Theming

The highlighter uses the following tags in Inform 7:

- `blockComment` for comments
- `bracket` for `(-` and `-)` surrounding I6 inclusions
- `definitionKeyword` for table names (might change in the future)
- `escape` for text substitutions (might change in the future)
- `heading1` for the story title (e.g. `"My awesome IF" by Natrium729`)
- `heading2` for `Volume` headings
- `heading3` for `Book` headings
- `heading4` for `Part` headings
- `heading5` for `Chapter` headings
- `heading6` for `Section` headings
- `invalid` for unpaired brackets in I7 texts and substitutions
- `string` for texts

Inside I6 inclusions, the following are used:

- `atom` for dictionary words
- `bool` for `true` and `false`
- `controlKeywords` for keywords (e.g. `if`, but those that are not really for control, like `new_line`, might be attributed another tag in the future)
- `escape` for escape sequences in strings (e.g. `~` or `@:e`)
- `float` for floats (e.g. `$+9.3e-2`)
- `integer` for numbers, binary numbers (e.g. `$$10`), hex number (e.g. `$E9`) and characters (e.g. `'n'`)
- `lineComment` for comments
- `null` for `nothing`
- `operatorKeyword` for e.g. `has` or `ofclass`
- `processingInstruction` for directives (might change in the future)
- `self` for `self`
- `string` for strings

## For developers

The syntax highlighting is implemented with a stream parser in `src/index.ts`.

To build, use:

```
$ npm run prepare
```

To test the highlighting in a CodeMirror editor, use:

```
$ npm run build-preview
```

Then you can open the file `preview/index.html` in your favourite browser and type some Inform 7 code.

## License

MIT. See the file `LICENSE` at the root of the repository.
